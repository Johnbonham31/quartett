import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DealService } from '../deal.service';

@Component({
  selector: 'app-single-card',
  templateUrl: './single-card.page.html',
  styleUrls: ['./single-card.page.scss'],
})
export class SingleCardPage implements OnInit {
  
  name: string;
  birthYear: number;

  constructor(public activatedRoute: ActivatedRoute, public dealService: DealService) { }

  ngOnInit() {
    const id: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    let display: any = this.dealService.getElement(id);

    this.name = display.name;
    this.birthYear = display.birthYear;
  }

}
