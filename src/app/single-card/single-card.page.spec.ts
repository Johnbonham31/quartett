import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SingleCardPage } from './single-card.page';

describe('SingleCardPage', () => {
  let component: SingleCardPage;
  let fixture: ComponentFixture<SingleCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SingleCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
