import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SingleCardPageRoutingModule } from './single-card-routing.module';

import { SingleCardPage } from './single-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SingleCardPageRoutingModule
  ],
  declarations: [SingleCardPage]
})
export class SingleCardPageModule {}
