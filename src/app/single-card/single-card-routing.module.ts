import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleCardPage } from './single-card.page';

const routes: Routes = [
  {
    path: '',
    component: SingleCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingleCardPageRoutingModule {}
