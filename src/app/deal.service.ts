import { Injectable } from '@angular/core';
import { Card } from 'src/card';


@Injectable({
  providedIn: 'root'
})
export class DealService {

  private items: Array<Card> =[];

  constructor() { }
  
  public getItems(): Array<Card> {
    return this.items;
  }

  getElement(id: number) {
    return id;
  }
}
