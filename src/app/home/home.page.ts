import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  
  cards: [];

  ngOnInit(){
    fetch('../../assets/data/cards.json').then(res => res.json()).then(json => {
      this.cards = json;
    });
  }

  constructor() {}

}
