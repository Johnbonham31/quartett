export class Card {
    id: number;
    name: string;
    birthYear: number;


    constructor(newId: number, newName: string, newBirthYear: number){
        this.id = newId;
        this.name = newName;
        this.birthYear = newBirthYear;

    }
}